# SQL Study Env

## Summary

This is a docker based environment to study sql.

## Requirements

- Docker
- Docker Compose

## Usage

First of all, let's build docker image and start sql servers.

```shell
docker-compose up -d --build
```

Then, you can access sql servers via client containers.

When you enter postgresql client,


```shell
docker-compose exec pgsql_client psql
```

you get prompt like as below.

```
$ docker-compose exec pgsql_client psql
psql (10.5)
Type "help" for help.

dev=#
```

Or, when you enter mysql client,

```shell
docker-compose sxec mysql_client mysql -u dev
```

you get prompt like as below.

```shell
$ docker-compose exec mysql_client mysql -u dev
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 8
Server version: 8.0.12 MySQL Community Server - GPL

Copyright (c) 2000, 2018, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
```
