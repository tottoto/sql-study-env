FROM alpine:latest
RUN apk --update add postgresql-client bash && \
    rm -rf /var/cache/apk/*
