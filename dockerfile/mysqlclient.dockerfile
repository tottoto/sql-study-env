FROM ubuntu:18.04
RUN apt-get update && \
    apt-get install -y mysql-client && \
    apt-get clean && \
    rm -rf /var/cache/apt/archives/* \
           /var/lib/apt/lists/*
